using System;
using System.IO;
using System.Xml;

namespace Redshape.Core
{
	public sealed class XmlHelper
	{
		public XmlHelper ()
		{
		}
		
		public static XmlDocument buildDocument( String path ) {
			return buildDocument( new FileInfo(path).Open(FileMode.Open) );
		}
		
		public static XmlDocument buildDocument( Stream file ) {
			XmlDocument document = new XmlDocument();
			document.Load( file );
			
			return document;
		}
	}
}

