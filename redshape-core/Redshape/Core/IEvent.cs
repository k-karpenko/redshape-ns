using System;

namespace Redshape.Core
{
	public interface IEvent
	{
		
		EventType getType();
		
		T[] getArgs<T>();
		
		T getArg<T>( int i );
		
	}
}

