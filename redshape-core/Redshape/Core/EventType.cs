using System;

namespace Redshape.Core
{
	[Serializable]
	public class EventType
	{
		public String type {
			get {
				return this.type;
			}
			
			set {}
		}
		
		protected EventType ( String type )
		{
			this.type = type;
		}
		
	}
}

