using System;

namespace Redshape.Core
{
	public interface IDispatcher
	{
		
		void addListener( EventType type, IEventListener listener );
		
	}
}

