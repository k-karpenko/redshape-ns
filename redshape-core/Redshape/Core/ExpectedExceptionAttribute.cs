using System;
namespace Redshape.Core
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class ExpectedExceptionAttribute : System.Attribute
	{
		private Type type;
		
		public ExpectedExceptionAttribute ( Type exceptionType ) {
			this.type = exceptionType;
		}
	}
}

