using System;
using System.Collections.Generic;

namespace Redshape.Core
{
	public class Dispatcher : IDispatcher
	{
		private IDictionary<EventType, List<IEventListener>> listeners =
					new Dictionary<EventType, List<IEventListener>>();
		
		public Dispatcher ()
		{}
		
		public void addListener( EventType type, IEventListener listener ) {
			List<IEventListener> listeners;
			this.listeners.TryGetValue( type, out listeners );
			if ( listeners == null ) {
				this.listeners.Add( type, listeners = new List<IEventListener>() );
			}
			
			listeners.Add( listener );
		}
		
		protected void fireEvent( IEvent eventObject ) {
			List<IEventListener> listeners;
			if ( this.listeners.TryGetValue( eventObject.getType(), out listeners )) {
				throw new Exception("Event not supported");
			}
			
			foreach ( IEventListener type in listeners ) {
				type.handle( eventObject );
			}	
		}
	}
}

