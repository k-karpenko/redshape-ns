using System;

namespace Redshape.Core
{
	public interface IEventListener
	{
		
		void handle( IEvent eventObject );
		
	}
}

