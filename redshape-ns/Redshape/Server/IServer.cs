using System;
namespace Redshape.Server
{
	public interface IServer : IDispatcher
	{
		
		void start();
		
		void stop();
		
		/**
		 * Must be removed from interface
		 */
		ApplicationContext getContext();
		
	}
}

