using System;
namespace Redshape.Server
{
	public class ServerEvent : EventType
	{
		
		public ServerEvent ( String message )
		{
			EventType(message);
		}
		
		public static sealed ServerEvent Start = new ServerEvent("Server.Event.Start");
		
		public static sealed ServerEvent Stop = new ServerEvent("Server.Event.Stop");
		
		public static sealed ServerEvent Request = new ServerEvent("Server.Event.Request");
		
	}
}

