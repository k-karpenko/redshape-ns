using System;

using Redshape.Core;
using Redshape.Context;

namespace Redshape
{
	public class Test
	{

		public static void Main( string[] args ) {
			if ( args.Length != 1 ) {
				throw new Exception("Context path not provided!");
			}
			
			ApplicationContext context = new ClassPathXMLContext( args[0] );
			
			IServer server = context.getBean( typeof(IServer) );
			server.addListener( ServerEvents.Start, new WebServerListener() );
			
			server.start();
		}
	}
}

