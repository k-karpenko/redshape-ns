using System;
namespace Redshape.Context
{
	public class ProcessingException : Exception
	{
		public ProcessingException () : base() {}
		
		public ProcessingException( String message ) : base(message) {} 
		
		public ProcessingException( String message, Exception exception ) 
				: base(message, exception) {}
		
	}
}

