using System;
namespace Redshape.Context
{
	public interface ApplicationContext
	{
		
		void autowire( Object target );
		
		T getBean<T>( Type type );
		
		T getBean<T>( String name );
		
		void refresh();
		
	}
}

