using System;
namespace Redshape.Context
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field
	                	| AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class AutowiredAttribute : System.Attribute
	{
		bool required;
		
		public AutowiredAttribute( bool required )
		{
			this.required = required;
		}
		
	}
}

