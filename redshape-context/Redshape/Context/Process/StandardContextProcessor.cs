using System;
using System.Xml;

using Redshape.Core;

namespace Redshape.Context.Process
{
	public class StandardContextProcessor : IContextProcessor
	{
		public StandardContextProcessor ()
		{
		}
		
		[ExpectedException(typeof(ProcessingException))]
		public void process( IContextHolder holder ) {
		}
	}
}

