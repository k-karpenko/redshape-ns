using System;
using System.Collections.Generic;
using System.Xml;

namespace Redshape.Context.Process
{
	public class XmlContextItem : IContextItem
	{
		private XmlElement element;
		private IContextHolder holder;
		
		public XmlContextItem ( IContextHolder holder, XmlElement element )
		{
			this.element = element;
			this.holder = holder;
		}
		
		public String getParameter( String name ) {
			return this.element.Attributes.GetNamedItem(name).Value;
		}
		
		public IContextItem getParent() {
			return new XmlContextItem( this.holder, (XmlElement) this.element.ParentNode );
		}
		
		public IContextHolder getHolder() {
			return this.holder;
		}
		
		public ContextItemType getType() {
			return ContextItemType.valueOf( this.element.Name );
		}
		
		public String getValue() {
			return this.element.Value;
		}
		
		public List<IContextItem> childs() {
			return this.childs( null );
		}
		
		public List<IContextItem> childs( String name ) {
			List<IContextItem> result = new List<IContextItem>();
			foreach ( XmlNode node in this.element.ChildNodes ) {
				if ( node is XmlElement ) {
					if ( name == null || name.Equals( node.Name ) ) {
						result.Add( new XmlContextItem( this.holder, (XmlElement) node ) );
					}
				}
			}
					           
		    return result;			       
		}
		
	}
}

