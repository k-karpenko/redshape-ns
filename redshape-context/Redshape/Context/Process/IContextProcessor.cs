using System;
using Redshape.Core;

namespace Redshape.Context.Process
{
	public interface IContextProcessor
	{
		
		[ExpectedException(typeof(ProcessingException))]
		void process( IContextHolder holder );
		
	}
}

