using System;
namespace Redshape.Context.Process
{
	/**
	 * Represent set of IContextHolderItem objects which
	 * represents formal context structure.
	 * Holds all context and provides methods to retreiving
	 * object by a few set of traits:
	 *   a) ID
	 *   b) Subset by a given top edge and bottom edge
	 */
	public interface IContextHolder
	{
		
		IContextItem[] getAll();
		
		IContextItem getItemById( String id );
		
		IContextItem getSubset( IContextItem top, IContextItem bottom );
		
	}
}

