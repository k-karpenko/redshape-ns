using System;
using System.Collections.Generic;
using Redshape.Context;

namespace Redshape.Context.Process
{
	public interface IContextItem
	{
		
		IContextHolder getHolder();
		
		ContextItemType getType();
		
		String getParameter( String name );
		
		String getValue();
		
		List<IContextItem> childs();
		
		List<IContextItem> childs( String name );
		
	}
}

