using System;
using Redshape.Core;
using Redshape.Context.Process;

namespace Redshape.Context.Process.Visitors
{
	public interface IContextNodeHandler
	{
		
		/**
		 * Deal with declarations node processing
		 * since handler implementation knows all about
		 * what to do with node attributes and childs placed under
		 * it.
		 */
		[ExpectedException(typeof(ProcessingException))]
		void handle( IContextItem item );
		
		/**
		 * Check that node type supports by 
		 * current visitor implementation
		 * 
		 * @return bool
		 */
		bool check( ContextItemType type );
		
	}
}

