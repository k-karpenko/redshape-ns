using System;
namespace Redshape.Context.Process.Visitors
{
	public class BeansNodeVisitor
	{
		public BeansNodeVisitor ()
		{
		}
		
		public bool check( ContextItemType type ) {
			return type.Equals( ContextItemType.BEANS );
		}
		
		public void handle( IContextItem item ) {
		}
		
	}
}

