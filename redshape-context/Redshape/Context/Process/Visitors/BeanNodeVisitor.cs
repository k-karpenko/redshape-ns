using System;
namespace Redshape.Context.Process.Visitors
{
	public class BeanNodeVisitor : IContextNodeHandler
	{
		public BeanNodeVisitor ()
		{
		}
		
		public bool check( ContextItemType type ) {
			return type.Equals( ContextItemType.BEAN );
		}
		
		public void handle( IContextItem item ) {
			
		}
		
	}
}

