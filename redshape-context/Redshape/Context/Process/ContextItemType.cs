using System;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.CompilerServices;

using Redshape.Core;

namespace Redshape.Context.Process
{
	[Serializable]
	public class ContextItemType : IEnum {
		private String _name;
		
		protected ContextItemType( String name ) {
			this._name = name;
			REGISTRY.Add( name, this );
		}
		
		private static readonly Dictionary<String, ContextItemType> REGISTRY 
								= new Dictionary<String, ContextItemType>();
		
		public static readonly ContextItemType BEAN = new ContextItemType("bean");
		public static readonly ContextItemType BEANS = new ContextItemType("beans");
		public static readonly ContextItemType ARRAY = new ContextItemType("array");
		public static readonly ContextItemType VALUE = new ContextItemType("value");
		public static readonly ContextItemType REF = new ContextItemType("ref");
		public static readonly ContextItemType MAP = new ContextItemType("map");
		public static readonly ContextItemType ENTRY = new ContextItemType("entry");
		public static readonly ContextItemType SET = new ContextItemType("set");
		
		public static ContextItemType valueOf( String name ) {
			ContextItemType result;
			if ( REGISTRY.TryGetValue( name, out result ) ) {
				return result;
			} else {
				throw new ArgumentException("Name not related an any of context types");
			}
		}
		
		public static ContextItemType[] values() {
			ContextItemType[] result = new ContextItemType[ContextItemType.REGISTRY.Count];
			
			int i = 0;
			foreach ( ContextItemType @value in REGISTRY.Values ) {
				result[i++] = @value;
			}
			
			return result;
		}
		
		public String name() {
			return this._name;
		}
	}
}

