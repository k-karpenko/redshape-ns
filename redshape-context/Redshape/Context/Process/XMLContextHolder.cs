using System;
using System.Collections.Generic;
using System.Xml;

using Redshape.Context.Process.Visitors;

namespace Redshape.Context.Process
{
	public class XMLContextHolder : IContextHolder
	{
		private Dictionary<ContextItemType, IContextNodeHandler> handlers 
				= new Dictionary<ContextItemType, IContextNodeHandler>();
		private XmlDocument document;
		private XmlElement rootNode;
		private IResourcesLoader loader;
		
		public XMLContextHolder ( XmlDocument document ) 
			: this ( document, new StandardResourcesLoader() ) {}
		
		public XMLContextHolder( XmlDocument document, IResourcesLoader loader ) {
			this.document = document;
			this.rootNode = this.document.DocumentElement;
			this.loader = loader;
		}
		
		public IContextItem[] getAll() {
			return new IContextItem[0];
		}
		
		public IContextItem getSubset( IContextItem top, IContextItem bottom ) {
			return null;
		}
		
		public IContextItem getItemById( String id ) {
			return new XmlContextItem( this, this.document.GetElementById( id ) );
		}
		
	}
}