using System;
using System.Collections.Generic;
using System.IO;

using Redshape.Core;

namespace Redshape.Context
{
	public interface IResourcesLoader
	{
		
		/**
		 * Load file if it's exists in
		 * added search locations
		 * 
		 * @return File
		 * @throws IOException
		 */
		[ExpectedException(typeof(System.IO.IOException))]
		FileInfo load( string path );
		
		void addSearchPath( String path );
		
		List<DirectoryInfo> getSearchPath();
		
		T getAttribute<T>( ResourcesLoaderAttributes name );
		
		void setAttribute( ResourcesLoaderAttributes attribute, Object value );
		
	}
}

