using System;
using System.IO;
using System.Collections.Generic;

using Redshape.Core;

namespace Redshape.Context
{
	public abstract class AbstractResourcesLoader : IResourcesLoader
	{
		private List<DirectoryInfo> searchPath = new List<DirectoryInfo>();
		
		private Dictionary<ResourcesLoaderAttributes, Object> attributes 
					= new Dictionary<ResourcesLoaderAttributes, Object>();
		
		internal AbstractResourcesLoader() 
			: this( new Dictionary<ResourcesLoaderAttributes, Object>() )
		{}
		
		internal AbstractResourcesLoader( Dictionary<ResourcesLoaderAttributes, Object> attributes )
			: this( attributes, new List<String>() ) {}
		
		internal AbstractResourcesLoader( List<String> searchPath ) 
			: this( new Dictionary<ResourcesLoaderAttributes, Object>(), searchPath ) {}
		
		internal AbstractResourcesLoader( Dictionary<ResourcesLoaderAttributes, Object> attributes,
			                     List<String> Path ) {
			this.attributes = attributes;
			
			this.initSearchPath(Path);
		}
			
		protected void initSearchPath( List<String> searchPath ) {
			foreach ( String path in searchPath ) {
				this.addSearchPath( path );
			}
		}
		
		public void addSearchPath( String path ) {
			DirectoryInfo pathObject = new DirectoryInfo(path);
			if ( !pathObject.Exists ) {
				throw new IOException("Provided search path not exists");
			}
			
			this.searchPath.Add( pathObject );
		}
		
		public List<DirectoryInfo> getSearchPath() {
			return this.searchPath;
		}
		
		public void setAttribute( ResourcesLoaderAttributes attribute, Object value ) {
			this.attributes.Add( attribute, value );
		}
		
		public T getAttribute<T>( ResourcesLoaderAttributes attribute ) {
			Object result;
			if ( this.attributes.TryGetValue( attribute, out result ) ) {
				return (T) result;
			}
			
			return default(T);
		}
		
		public abstract FileInfo load( String path );
		
	}
}

