using System;
using System.IO;
using System.Xml;

using Redshape.Context.Process;
using Redshape.Core;

namespace Redshape.Context
{
	public class ClasspathApplicationContext : ApplicationContext
	{
		private IResourcesLoader resourcesLoader;
		private IContextHolder contextHolder;
		private IContextProcessor processor;
		private String contextPath;
		
		public ClasspathApplicationContext( string declarationPath ) 
			: this( declarationPath, new StandardResourcesLoader() ) {}
		
		public ClasspathApplicationContext( string declarationPath, IResourcesLoader resourcesLoader ) 
			: this(declarationPath, resourcesLoader, new StandardContextProcessor() ) {}
		
		public ClasspathApplicationContext( string declarationPath, IResourcesLoader resourcesLoader,
		                                   							IContextProcessor processor ) {
			if ( declarationPath == null 
			    	|| resourcesLoader == null
			    		|| processor == null ) {
				throw new Exception("<null>");
			}
			
			this.resourcesLoader = resourcesLoader;
			this.contextPath = declarationPath;
			this.processor = processor;
			
			this.initContext();
		}
		
		public void autowire( object target ) {
		}
		
		protected void initContext() {
			try {
				this.contextHolder = new XMLContextHolder( 
				                      XmlHelper.buildDocument(this.contextPath),
				                      this.resourcesLoader
				                   );
				
				this.processor.process( this.contextHolder );
			} catch ( XmlException e ) {
				throw new IOException("Unable to read context declaration", e);
			} catch ( ProcessingException e ) {
				throw new IOException("Unable to process context!", e );
			}
		}
		
		public void refresh() {
		}
		
		public T getBean<T>( String beanName ) {
			return default(T);
		}
		
		public T getBean<T>( Type type ) {
			return default(T);		
		}
		
	}
}

