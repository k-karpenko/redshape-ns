using System;
using System.Collections.Generic;
using System.IO;

namespace Redshape.Context
{
	public class StandardResourcesLoader : AbstractResourcesLoader
	{
		
		
		public StandardResourcesLoader() : this( new List<String>() ) {}
		
		public StandardResourcesLoader( List<String> searchPath ) : base(searchPath) {}
		
		public override FileInfo load( String path ) {
			FileInfo result = null;
			if ( this.getSearchPath().Count == 0 ) {
				return result;
			}
			
			foreach ( DirectoryInfo searchPath in this.getSearchPath() ) {
				result = this.load( searchPath, path, 
					this.getAttribute<Boolean>( ResourcesLoaderAttributes.DeepSearch ) );	                 	
				if ( result != null ) {
					return result;
				}
			}
			
			return result;
		}
		
		protected FileInfo load( DirectoryInfo searchContext, String path, Boolean deepSearch ) {
			FileInfo[] info = searchContext.GetFiles( path );
			if ( info.Length != 0 ) {
				return info[0];
			}
				
			if ( !deepSearch ) {
				return null;
			}
			
			FileInfo result = null;
			foreach ( DirectoryInfo subContext in searchContext.GetDirectories() ) {
				result = this.load( subContext, path, true );
				if ( result != null ) {
					break;
				}
			}
			
			return result;
		}
		
	}
}

